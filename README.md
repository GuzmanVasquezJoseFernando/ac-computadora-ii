# ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB

## Computadoras Electrónicas

```plantuml
@startmindmap
* Computadoras electrónicas
** Colossus Mark 1
*** Diseñada por Tommy Flowers finalizada en diciembre de 1943
*** Ayudava a decodificabar las comunicaciones nazis
*** Tenia 1600 tuvos de vacio
*** Primera computadora electrónica programable
**** mediante la conección cientos de cables en un tablero

** ENIAC
*** Calculadora Integradora númerica electrónica
*** Diseñada por John Mauchly y J.Presper eckert
*** Contruida en la universidad de Pensilvania en 1946
*** Primera computadora electrónica programable de propósito general
**** Realizava 5000 sumas y restas de 10 digítos por segundo

@endmindmap
```

## Arquitectura Von Neumann y Arquitectura Harvard

```plantuml
@startmindmap

* Arquitectura Von Neumann y Arquitectura Harvard
** Ley de Moore
*** Electrónica
**** El número de transistores por chip se duplica cada año
**** cada 18 meses se duplica la potencia de calculo sin modificar el costo
*** Performance
**** Incrementa la velocidad del procesador
**** Incrementa la capacidad de la memoria
**** La velocidad de la memoria menor que la velocidad del procesador
** Arquitectura Von Neumann
*** Modelo
**** datos y programas se almacenan en una misma memoria de lectura-escritura
**** Los contenidos de esta memoria se acceden indicando su posición sin importar su tipo
**** Ejecución en sentencia (salvo que se indique lo contrario)
**** Representación binaria
*** Contiene
**** CPU
***** Unidad de Control (CPU)
***** Unidad Aritmética Lógica
***** Registros
**** Memoria Principal
***** Puede almacenar tanto instrucciones como datos
**** Sistema de Entrada/Salida
*** Surge el concepto de programa almacenado
*** Problema
**** Cuello de botella
***** CPU ociosa
*** Los componentes se comunican a tráves de un sistema de buses
**** Bus de datos
**** Bus de Direcciones
**** Bus de Control
** Arquitectura Harvard
*** Modelo
**** se referia a las computadoras que utilizaban almacenamiento fisicamente separado
***** Instrucciones
***** Datos
*** Contiene
**** CPU
***** Unidad de control
***** Unidad aritmética logíca
***** Registros
**** Memoria Principal
***** Almacena
****** Instrucciones
****** Datos
****** Sistema de S/E
*** Uso
**** Desarrollo de productos
**** Sistemas de propósito específico
***** Electrodomésticos
***** Mouse
***** Automóviles

@endmindmap
```

## Basura Electronica

```plantuml
@startmindmap

* Basura Electrónica
** Residuos de Aparatos Eléctricos y Electronicos (RAEEE)
*** 2012
**** Se aprobó Reglamento Nacional para la Gestión y Manejo de Aparatos Eléctricos y Electronicos
***** Establece derechos y obligaciones para la adecuada gestion y manejo de las RAEE
*** Metales
**** Oro
**** Cobre
**** Plata
**** Cobalto
*** Plástico
*** Vidrio
*** Residuos peligrosos
**** Pilas
**** Botones
** Materiales recuperados
*** Estaño
*** Silicio
*** Hierro
*** Aluminio
*** Plastico
*** Reduce el precio de la construcción de nuevos sistemas
*** pesar para calcular el pago
*** desarmarlo y clasificarlo
*** segregación y limpieza de las piezas
*** Almacenar con su clasificación
*** Exportación a la matriz
**** Fundición y recuperación de los metales

@endmindmap
```